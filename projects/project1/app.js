new Vue({
    el: "#app",
    data: {
        pcHealth: 100,
        monsterHealth: 100,
        log: [],
        gameRunning: false
    },
    methods: {
        attack: function() {
            var dmg = this.damage(3, 10);
            this.monsterHealth -= dmg;
            dmg = this.damage(5, 12);
            this.pcHealth -= dmg

            this.log.push({
                isUser: true,
                msg: 'You dealt ' + dmg + ' damage'
            });
        },
        damage: function(min, max) {
            return Math.max(Math.floor(Math.random() * max) + 1, min);
        },
        special: function() {

        },
        newGame: function() {
            this.pcHealth = 100;
            this.monsterHealth = 100;
            this.log = [];
            this.gameRunning = true;
        },
        heal: function() {
            if (this.pcHealth >= 100) {
                return
            }
            var amt = Math.floor(Math.random() * 10) + 3;
            if (this.pcHealth + amt > 100) {
                this.pcHealth += ((this.pcHealth + amt) - 100);
            } else {
                this.pcHealth += amt;
            }
            this.log.push({
                isUser: true,
                msg: 'You healed for ' + amt + ' HP.'
            });
            this.monsterHurt();
        },
        monsterHurt: function() {
            var amt = Math.floor(Math.random() * 10);
            this.pcHealth -= amt;
            this.log.push({
                isUser: false,
                msg: 'Monster dealt ' + amt + ' damage'
            });
        },
        turns: function() {
            this.hurt();
            this.monsterHurt();
        },
        giveUp: function() {

        }
    },
    computed: {

    },
    watch: {
        pcHealth: function() {
            if (this.pcHealth < 0) {
                this.gameEnd = true;
            }
        },
        monsterHealth: function() {
            if (this.monsterHealth < 0) {
                this.gameEnd = true;
            }
        },
        gameEnd: function() {
            if (this.gameEnd == true) {
                alert('Game Over');
            }
        }
    }
});